# ReveilMagique

Aide les enfants à savoir à quel moment se coucher et se lever en affichant une image différente selon des heures programmées. Ecrit en python/cgi/html/css/bootstrap.

Démonstration sur : [https://extrem-network.com/cgi/reveilmagique/](https://extrem-network.com/cgi/reveilmagique/)