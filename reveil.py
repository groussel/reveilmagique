#!/usr/bin/python3
import cgi
import datetime
from datetime import time

form = cgi.FieldStorage()
hcoucher =  form.getvalue('hcoucher')
mcoucher =  form.getvalue('mcoucher')
hlever =  form.getvalue('hlever')
mlever =  form.getvalue('mlever')
maintenant = datetime.datetime.now()

coucher = time(int(hcoucher), int(mcoucher))
lever = time(int(hlever), int(mlever))

def wheader():
	print('''Content-type: text/html; charset=UTF-8\n
    <html>
	<title>Reveil Magique</title>
	<meta charset='utf-8'>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<meta name='mobile-web-app-capable' content='yes'>
	<meta http-equiv='refresh' content='60'>
	<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css'>
  	<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
  	<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js'></script>
	''')

def wfooter():
	print('''</body>
    </html>	
	''')

def infos():
	print(f'''<div class='container'>
	<!-- Trigger the modal with a button -->
	<button type='button' class='btn btn-success btn-xs' data-toggle='modal' data-target='#myModal'>infos</button>
	<!-- Modal -->
	<div class='modal fade' id='myModal' role='dialog'>
	<div class='modal-dialog modal-sm'>
	<div class='modal-content'>
	<div class='modal-header'>
	<button type='button' class='close' data-dismiss='modal'>&times;</button>
	<h4 class='modal-title'>Informations</h4>
	</div>
    <div class='modal-body'>
	<p>Heure du coucher : {coucher}<br>
	Heure du reveil : {lever}<br>
	Heure de l'app : {maintenant.time()}<br>
	Actualisation toutes les 2 min.<hr> 
	<a href='index.html'><b>Programmer de nouveaux horaires</b></a><hr>
	<span style='font-size:12px'>Reveil Magique :-)<br>ecrit avec python/html/css/bootstrap par <a href='https://twitter.com/groussel' target='_blank'>@groussel</a></span><br>
	<span style='font-size:10px'>Mickey Mouse est une marque commerciale propriete de The Walt Disney Company.</span>
	</p>
	</div>
    <div class='modal-footer'>
    <button type='button' class='btn btn-default' data-dismiss='modal'>fermer</button>
    </div>
    </div>
    </div>
  	</div>
	</div>
	''')

def jour():
	print("<style>")
	print("body {background-image: url('images/wakeup.png'); background-repeat: no-repeat; background-attachment: fixed; background-position: center; background-color: blue; }")
	print("</style>")
	infos()

def nuit():
	print("<style>")
	print("body {background-image: url('images/sleep.png'); background-repeat: no-repeat; background-attachment: fixed; background-position: center; background-color: black; }")
	print("</style>")
	infos()

wheader()

if lever == coucher:
	print('''<br><br><br><div align='center'><b>Erreur !</b><br>
	L'heure de coucher ne peut pas etre egale a l'heure du reveil<br>
	<a href='index.html'>retour</a></div>
	''')

elif lever < coucher:
    if maintenant.time() >= coucher or maintenant.time() < lever:
        nuit()
    else:
        jour()
elif maintenant.time() >= coucher and maintenant.time() < lever:
    nuit()
else:
    jour()

wfooter()